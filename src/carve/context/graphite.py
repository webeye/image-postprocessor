"""
read data from graphite
"""

__author__ = 'rak'

import urllib
from urllib.error import  HTTPError, URLError
from urllib import request
import json
from http.client import  HTTPException
from carve.context.graphiteResource.builder.url import GraphiteUrlBuilder
from carve.context.graphiteResource.builder.dimension import DimensionBuilder
from carve.context.BaseContext import BaseContext, logger


class GraphiteException(Exception):
    pass


class graphite(BaseContext):

    _urlBuilder = None
    _response = None

    def __init__(self):
        self._urlBuilder = None
        self._response = None

    @property
    def url_builder(self) -> GraphiteUrlBuilder:
        if self._urlBuilder is None:
            self._urlBuilder = GraphiteUrlBuilder()
        return self._urlBuilder

    def cleanup(self) -> object:
        self._urlBuilder = None
        return self

    def format(self, format):
        self.url_builder.set_data_format(format)
        return self

    def prepare_context(self) -> dict:
        url = self.config.get_kv("{}/url".format(self.m))
        starttime = self.config.get_kv("{}/starttime".format(self.m))
        endtime = self.config.get_kv("{}/endtime".format(self.m)).format(**self.config.context)
        self.url_builder.reset_dimensions()
        self.url_builder.set_url_prefix(url)
        self.url_builder.set_start_time(starttime)
        self.url_builder.set_end_time(endtime)
        self.url_builder.set_data_format("json")
        dims = []
        print(self.m)
        for name in self.config.get_kvs("{}/dimensions".format(self.m)):
            d_str = self.config.get_kv("{}/dimensions/{}".format(self.m, name))
            print('name %s' % name)
            print('d_str %s' % d_str)
            if d_str:
                dims.append(name)
                self.url_builder.add_dimension(DimensionBuilder().set_dimension(d_str))

        if len(dims) == 0:
            logger.warning("No dimension found.")
            return self
        try:
            url_t = self.url_builder.url
            req = urllib.request.Request(url_t)
            req.add_header('Content-Type',  'application/json')
            with request.urlopen(req) as f:
                json_data = json.loads(f.read().decode("utf-8"))

            dims.reverse()
            for k in dims:
                try:
                    ln = json_data.pop()
                    v = list(filter(None, [v[0] for v in ln.get('datapoints')]))[-1]
                except Exception:
                    v = ""
                self.config.set_context(k, v)

        except HTTPError as e:
           logger.error("Error reading `{}`, {}".format(url_t, e))
        except  URLError as e:
           logger.error("Error reading `{}`, {}".format(url_t, e))
        except HTTPException as e:
            logger.error("Error reading `{}`, {}".format(url_t, e))

        return self

