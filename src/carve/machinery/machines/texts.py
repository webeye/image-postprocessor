import pytz
from carve.machinery.machines.BaseMachine import BaseMachine
from carve.machinery.BaseImage import logger
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import locale

class texts(BaseMachine):

    def carve(self)-> Image:
        for txt_id in self.config.get_kvs("{}/texts".format(self.cp)):

            font_size = self.config.get_kv("{}/texts/{}/font_size".format(self.cp, txt_id), 10)
            line_height = self.config.get_kv("{}/texts/{}/line_height".format(self.cp, txt_id), 10)
            x = self.config.get_kv("{}/texts/{}/x".format(self.cp, txt_id))
            y = self.config.get_kv("{}/texts/{}/y".format(self.cp, txt_id))
            font_file = self.config.get_kv("{}/texts/{}/font_file".format(self.cp, txt_id))
            text_locale = self.config.get_kv("{}/texts/{}/text_locale".format(self.cp, txt_id), 'cs_CZ.UTF-8')
            text_color = self.config.get_kv("{}/texts/{}/text_color".format(self.cp, txt_id), 0xFF00FF)
            text_color = self.config.context.get("text_color", text_color)
            timezone = self.config.context.get("{}/texts/{}/timezone", 'Europe/Prague')
            utc_dt = self.config.context.get('utc_dt', None)

            self.config.set_context("local_dt", utc_dt.astimezone(pytz.timezone(timezone)))

            position = (x, y)

            old_locale = locale.setlocale(locale.LC_ALL, '')
            locale.setlocale(locale.LC_ALL, text_locale)

            draw = ImageDraw.Draw(self.base_image.image)
            font = ImageFont.truetype(font_file, font_size, encoding='unic')
            i = 0
            for ln in self.config.get_kvs("{}/texts/{}/lines".format(self.cp, txt_id)):
                try:
                    tx = ln.format(**self.config.context)
                except KeyError as e:
                    logger.error(str(e))
                    tx = ln
                draw.text([position[0], position[1] + i * line_height], tx, text_color, font=font )
                i += 1

            locale.setlocale(locale.LC_ALL, old_locale)

        return self.base_image.image